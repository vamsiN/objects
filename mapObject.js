function mapObject(obj, cb) {
   
    for(let key in obj){
        
        obj[key]= cb(key,obj[key])
    }
    return obj

}
module.exports = mapObject