const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
const mapObject = require('../mapObject')

function marvel(key,value){
    if(key == 'name'){
        return 'Tony Stark'
    }else if(key == 'age'){
        return 40
    }else if (key == 'location'){
        return 'New York'
    }

}

const result1 = mapObject(testObject,marvel )
console.log(result1)


const testObject2 = {start: 5, end: 12}

function add (key,value){
    return value+5;
}

const result2 = mapObject(testObject2,add)
console.log(result2)