const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; // use this object to test your functions
const defaults = require('../defaults')

const result1 = defaults(testObject, {country:"india"})
console.log(result1)


const testObject2 = {a:1, b:2, c:3}
const result2 = defaults(testObject2,{c:30, d:40})
console.log(result2)