function invert(obj) {
    
    let inverted_obj = {}

    for(let key in obj){
        value = obj[key]
        inverted_obj[value] = key;
    }

    return inverted_obj
}

module.exports = invert