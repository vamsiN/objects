function defaults(obj, defaultProps) {
    let new_obj = obj

    for(let item in defaultProps){
        new_obj[item] = defaultProps[item]
    }
    return new_obj
}

module.exports = defaults